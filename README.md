>### Frontend project with React+Redux by Alejandro Vañó

## Getting started

To get the frontend running locally:

- Clone this repo
- `npm install` to install all req'd dependencies
- `npm start` to start the local server (this project uses create-react-app)

Local web server will use port 4100 instead of standard React's port 3000 to prevent conflicts with some backends like Node or Rails. You can configure port in scripts section of `package.json`.

### Making requests to the backend API

Server is created with MoleculerJS + GraphQL + Prisma, if you want to try to join the frontend with the backend, you can clone it [here.](https://gitlab.com/Avato92/backend_moleculerjs_graphql_prisma)

If you want to change the API URL to a local server, simply edit `src/agent.js` and change `API_ROOT` to the local server's URL (i.e. `http://localhost:3000/api`)


## Functionality overview

In this example we simulate that we have a list of sports, with their categories and their teams, each team will have its players, its coaching staff and its owner

**General functionality:**

- Authenticate users via JWT (login/signup pages + logout button on settings page)
- GET and display paginated lists of teams
- Dependent dropdowns Sports and categories
- List teams with filters and sorteable
- Modal with the details of the teams (their players)
- All the modules have been composed in smaller components (components containers and presentational components)
- I18n (English, Spanish and Valencian)
- PropTypes
- Set Cookies and Get Cookies(Save language, when you open again the browser search your language for cookies and if you do not have it defined assign one by default)
- Contact

**The general page breakdown looks like this:**

- Sign in/Sign up pages (URL: /#/login, /#/register )
    - Use JWT (store the token in localStorage)
- Teams page (URL : /teams)
    - List of sports
    - List categories filtered by sport
    - List teams filtered by sport and category, and the result is sorteable
- Contact (URL : /contact)
    - Form
    - Send email

 **TO DO:**

    - Social Login
    - Header composed
    - More translate

<br />

 
