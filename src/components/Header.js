import React from 'react';
import { Link } from 'react-router-dom';
import {Select} from 'semantic-ui-react';
import {Utils} from '../utils';
import { setLanguage } from "redux-i18n";
import { connect } from 'react-redux';

const LoggedOutView = props => {
  const countryOptions = [
    {key: 'es', value: 'es', flag: 'es', text: 'Español' },
    {key: 'en', value: 'en', flag: 'gb', text: 'English'},
    {key: 'va', value: 'ca', flag: 'pr', text: 'Valencià'}
  ];
  if (!props.currentUser) {
    return (
      <ul className="nav navbar-nav pull-xs-right">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/login" className="nav-link">
            Sign in
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/register" className="nav-link">
            Sign up
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/contact" className="nav-link">
            Contact
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/teams" className="nav-link">
            Teams
          </Link>
        </li>
        <li className="nav-item">
          <Select placeholder='Select your country' options={countryOptions} defaultValue='es' onChange={ (e, {value}) => {props.selectLang(value)}}/>
        </li>
      </ul>
    );
  }
  return null;
};

const LoggedInView = props => {
  if (props.currentUser) {
    return (
      <ul className="nav navbar-nav pull-xs-right">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/editor" className="nav-link">
            <i className="ion-compose"></i>&nbsp;New Post
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/settings" className="nav-link">
            <i className="ion-gear-a"></i>&nbsp;Settings
          </Link>
        </li>

        <li className="nav-item">
          <Link
            to={`/@${props.currentUser.username}`}
            className="nav-link">
            <img src={props.currentUser.image} className="user-pic" alt={props.currentUser.username} />
            {props.currentUser.username}
          </Link>
        </li>

      </ul>
    );
  }

  return null;
};

class Header extends React.Component {
  constructor(props){
    super(props);
    this.selectLang = ev =>{
      this.props.selectLang(ev);
      Utils.setUserLanguage(ev);
      }
  }
  render() {
    return (
      <nav className="navbar navbar-light">
        <div className="container">

          <Link to="/" className="navbar-brand">
            {this.props.appName.toLowerCase()}
          </Link>

          <LoggedOutView currentUser={this.props.currentUser} selectLang={this.selectLang} />

          <LoggedInView currentUser={this.props.currentUser} />
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  value: Utils.getCookie("language")
});

const mapDispatchToProps = dispatch => ({
  selectLang: value => {
    dispatch(setLanguage(value));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);