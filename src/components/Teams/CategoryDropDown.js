import React from 'react';
import { Select } from 'semantic-ui-react';
import PropTypes from 'prop-types';
/**
 * CategoryDropDown component
 * He receives by props all the sports and the function to dispatch
 */

class OptionsCategories extends React.Component{
    
    render(){
      return  <Select size="3"
            onChange={(e,{value}) => this.props.listTeams(value)}
            placeholder={this.context.t('selectCategory')}
            options={this.props.categories}
            value={this.props.categories.value}
            />
    }
}
export default OptionsCategories;

OptionsCategories.propTypes= {
    categories: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.string.isRequired,
        key: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
    })),
    listTeams: PropTypes.func.isRequired
}
OptionsCategories.contextTypes = {
    t: PropTypes.func.isRequired
}