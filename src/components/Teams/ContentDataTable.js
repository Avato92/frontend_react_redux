import React from 'react';
import { Table } from 'semantic-ui-react';
import DetailsTeam from './DetailsTeam';
import PropTypes from 'prop-types';
/**
 * @component ContentDataTable, makes a loop and creates all rows taken from database
 */
class ContentDataTable extends React.Component {

    render() {
        return (
            <Table.Body>
                {this.props.data.map((team) => {
                    return (
                        <Table.Row>
                            <DetailsTeam
                                team={team}
                                listPlayers={this.props.listPlayers}
                                player={this.props.player}
                                pages={this.props.pages}
                                changePage={this.props.changePage}
                            />
                            <Table.Cell>{team.wins}</Table.Cell>
                            <Table.Cell>{team.draws}</Table.Cell>
                            <Table.Cell>{team.defeats}</Table.Cell>
                            <Table.Cell>{team.points}</Table.Cell>
                        </Table.Row>
                    )
                })}

            </Table.Body>
        )
    }
}
export default ContentDataTable;

ContentDataTable.propTypes= {
    data: PropTypes.arrayOf(PropTypes.shape({
        team: PropTypes.string.isRequired,
        wins: PropTypes.number.isRequired,
        defeats: PropTypes.number.isRequired,
        draws: PropTypes.number.isRequired,
        points: PropTypes.number.isRequired,
        id: PropTypes.string.isRequired
    })),
    listPlayers: PropTypes.func,
    player: PropTypes.object,
    pages: PropTypes.number,
    changePage: PropTypes.func
}