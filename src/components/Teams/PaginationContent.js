import React from 'react';
import PropTypes from 'prop-types';
/**
 * @component PaginationContent, print the pagination and the functions to change page
 */
class PaginationContent extends React.Component {
    constructor(props){
        super(props);
        this.state={
            currentPage:0
        }
    }
    setPage = page => {
          this.setState({
              currentPage: page
          })
          this.props.changePage(page, this.props.team_id);
      };

    render() {
        const range = [];
        for (let i = 0; i < this.props.pages; ++i) {
          range.push(i);
        }
        return (

            <nav>
            <ul className="pagination">
            
              {
                range.map(v => {
                  const isCurrent = v === this.state.currentPage;
                  const onClick = ev => {
                    ev.preventDefault();
                    this.setPage(v);
                  };
                  return (
                    <li
                      className={ isCurrent ? 'page-item active' : 'page-item' }
                      onClick={onClick}
                      key={v.toString()}>
      
                      <a className="page-link" href="">{v + 1}</a>
      
                    </li>
                  );
                })
              }
      
            </ul>
          </nav>
        )
    }
}
export default PaginationContent;

PaginationContent.propTypes= {
  pages: PropTypes.number,
  changePage: PropTypes.func,
  team_id: PropTypes.string
}