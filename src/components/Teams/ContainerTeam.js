import React from 'react';
import agent from '../../agent';
import { connect } from 'react-redux';
import {
  LIST_SPORTS,
  LIST_CATEGORIES,
  LIST_TEAMS,
  LIST_PLAYERS,
  UPDATE_DROPDOWN,
  CHANGE_PAGE
} from '../../constants/actionTypes';
/**
 * Import all the components
 */
import OptionsSports from './SportDropDown';
import OptionsCategories from './CategoryDropDown';
import ContainerList from './ContainerList';
/** 
 * Load all data
*/
const mapStateToProps = state => ({ ...state.teams });

const mapDispatchToProps = dispatch => ({
  /**
   * onLoad list all the sports in the first dropdown, when component is mounted
   * onChangeSport when user select a sport, we update the state and list all the categories from this sport
   * onChangeCategory very similar to onChangeSport, it list all the teams of this sport and category
   * onSelectTeam count the number of players to determine the size of the page and then remove the first player from the chosen team
   * onChangePage, request to the server to get the next player of selected team
   */
  onLoad: payload => {
    dispatch({ type: LIST_SPORTS, payload })
  },
  onChangeSport: value => {
    dispatch({ type: UPDATE_DROPDOWN, key: "sport", value: value })
    dispatch({ type: LIST_CATEGORIES, payload: agent.Teams.listCategories(value) })
  },
  onChangeCategory: value => {
    dispatch({ type: UPDATE_DROPDOWN, key: "category", value: value })
    dispatch({ type: LIST_TEAMS, payload: agent.Teams.listTeams(value) })
  },
  onSelectTeam: value => {
    dispatch({ type: LIST_PLAYERS, payload: agent.Teams.listPlayers(value), key: "team", value: value })
  },
  onChangePage: (value, team) => {
    dispatch({ type: CHANGE_PAGE, payload: agent.Teams.setPage(value, team) })
  }
});

class ContainerTeam extends React.Component {
  constructor() {
    super();
    /**
     * when user change we catch the value from select and send a server request with that info
     */
    this.listCategories = ev =>
      this.props.onChangeSport(ev);
    this.listTeams = ev =>
      this.props.onChangeCategory(ev);
    this.listPlayers = ev =>
      this.props.onSelectTeam(ev);
    this.changePage = (ev, team_id) =>
      this.props.onChangePage(ev, team_id);
    /**
     * We need this local state for sorteable table
     */
    this.state = {
      data: ""
    };
  };

  componentWillMount() {
    /**
     * A promise to load data(sports) when the component is render
     */
    this.props.onLoad(Promise.all([
      agent.Teams.listSports()
    ]));
  }
  /**
   * @param {state} next 
   * this function is needed because I need set new state when receive information about the teams to make sorteable the table
   */
  componentWillReceiveProps(next) {
    if (next.teams) {
      this.setState({ data: next.teams })
    }
  }
  /**render */
  render() {
    /** I need this two variables to push all the information and pass to the component*/
    let sports = [];
    let categories = [];
    /** when is in a promise, shows a Loading message*/
    if (this.props.isLoading) {
      return (
        <h1>Loading...</h1>
      )
    }
    /** if the server is down, show this message */
    else if (!this.props.sports) {
      return (
        <h1>No results...</h1>
      )
    }
    /** Push all sports in array */
    this.props.sports.forEach((sport) => sports.push({ key: sport.id, value: sport.id, text: sport.name }));
    /** if exist categories push all in array */
    (this.props.categories) ? this.props.categories.forEach((category) => categories.push({ key: category.id, value: category.id, text: category.name })) : null;
    /**
     * const before is needed because when compile, throw me a warning because expect a assigment, 
     * and the console log is because throw me another warning exp is declarate but never used
     */
    /**the container component, have 3 small components
     * 1 dependent dropdown with all sports
     * 1 depenent dropdown with all categories
     * and 1 datatable with another small components (bodytable and modal)
     */
    return (
      <div>
        <OptionsSports
          sports={sports}
          listCategories={this.listCategories}
        />
        {(categories.length > 0) ?
          <OptionsCategories
            categories={categories}
            listTeams={this.listTeams}
          />
          : null}
        {(this.props.teams) ?
          <ContainerList
            data={this.state.data}
            listPlayers={this.listPlayers}
            player={this.props.player}
            pages={this.props.pages}
            changePage={this.changePage}
          />
          : null}
      </div>

    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainerTeam);