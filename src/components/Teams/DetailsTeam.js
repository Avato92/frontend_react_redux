import React from 'react';
import { Modal, Image, Header, Table } from 'semantic-ui-react';
import PaginationContent from './PaginationContent';
import PropTypes from 'prop-types';
/**
 * @component DetailsTeam, print a modal with pagination and players details
 */
class DetailsTeam extends React.Component {

    render() {
        return (

            <Modal
                trigger={<Table.Cell>
                    <a
                        key={this.props.team.id}
                        onClick={(ev) => this.props.listPlayers(this.props.team.id)}
                    >
                        {this.props.team.team}
                    </a>
                </Table.Cell>}
                centered={false}
            >
                {(this.props.player) ?
                    <div>
                        <Modal.Header>{this.props.player.name}</Modal.Header>
                        <Modal.Content image>
                            <Image
                                wrapped size='medium'
                                src={this.props.player.photo}
                            />
                            <Modal.Description>
                                <Header>{this.props.team.team}</Header>
                                <p>{this.context.t('modal-position')} {this.props.player.position}</p>
                                <p>{this.context.t('modal-salary')} {this.props.player.salary} €</p>
                            </Modal.Description>
                            <PaginationContent
                                pages={this.props.pages}
                                changePage={this.props.changePage}
                                team_id={this.props.team.id}
                            />
                        </Modal.Content>
                    </div>
                    : null
                }

            </Modal>
        )
    }
}
export default DetailsTeam;

DetailsTeam.propTypes= {
    team: PropTypes.shape({
        team: PropTypes.string.isRequired,
        wins: PropTypes.number.isRequired,
        defeats: PropTypes.number.isRequired,
        draws: PropTypes.number.isRequired,
        points: PropTypes.number.isRequired,
        id: PropTypes.string.isRequired
    }),
    listPlayers: PropTypes.func,
    player: PropTypes.object,
    pages: PropTypes.number,
    changePage: PropTypes.func
}

DetailsTeam.contextTypes = {
    t: PropTypes.func.isRequired
}