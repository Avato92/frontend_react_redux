import React from 'react';
import { Select } from 'semantic-ui-react';
import PropTypes from 'prop-types';
/**
 * SportDropDown component
 * He receives by props all the sports and the function to dispatch
 */
class OptionsSports extends React.Component{
    
    render(){
        console.log(this.props.sports)
      return  <Select size="3"
            onChange={(e,{value}) => this.props.listCategories(value)}
            placeholder={this.context.t('selectSport')}
            options={this.props.sports}
            value={this.props.sports.value}
            />
    }
}
export default OptionsSports;

OptionsSports.propTypes= {
    sports: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.string.isRequired,
        key: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
    })),
    listCategories: PropTypes.func.isRequired
}
OptionsSports.contextTypes = {
    t: PropTypes.func.isRequired
}