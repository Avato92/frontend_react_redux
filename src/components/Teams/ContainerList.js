import React from 'react';
import { Table } from 'semantic-ui-react'
import _ from 'lodash';
import ContentDataTable from './ContentDataTable';
import PropTypes from 'prop-types';
/**
 * @component ContainerList have the header of the teable and small component with the bodytable and modal
 */
class ContainerList extends React.Component {
    /**
     * 
     * @param {*} props 
     * we need all the information about teams and players
     * and we need the state to make sorteable table
     */
    constructor(props) {
        super(props);
        this.state = {
            column: null,
            data: this.props.data,
            direction: null,
        }
    }
    /**
     * @function
     * this function sort the table by columns
     * first check if the column selected is at state
     * if it true, check the direction and reverse
     * it if false: set the column state and set the direction ascending
     */
    handleSort = clickedColumn => () => {
        const { column, data, direction } = this.state
        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                data: _.sortBy(data, [clickedColumn]),
                direction: 'ascending',
            })
            return
        }

        this.setState({
            data: data.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }
    /**render */
    render() {
        /**
         * this variables is information needed to make sorteable table
         */
        const { column, direction } = this.state;

        /**
         * This is the header table
         * and ContentDataTable is a small component with all rows and modal, 
         * I need pass all information about all the players and if is necesary the player selected at modal
         */
        return (
            <Table sortable celled fixed>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell
                            sorted={column === 'team' ? direction : null}
                            onClick={this.handleSort('team')}
                        >
                            {this.context.t('label-team')}
               </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={column === 'wins' ? direction : null}
                            onClick={this.handleSort('wins')}
                        >
                            {this.context.t('label-wins')}
              </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={column === 'draws' ? direction : null}
                            onClick={this.handleSort('draws')}
                        >
                            {this.context.t('label-draws')}
              </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={column === 'defeats' ? direction : null}
                            onClick={this.handleSort('defeats')}
                        >
                            {this.context.t('label-defeats')}
              </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={column === 'points' ? direction : null}
                            onClick={this.handleSort('points')}
                        >
                            {this.context.t('label-points')}
              </Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <ContentDataTable
                    data={this.state.data}
                    listPlayers={this.props.listPlayers}
                    player={this.props.player}
                    pages={this.props.pages}
                    changePage={this.props.changePage}
                />
            </Table>
        )
    }
}

export default ContainerList;

ContainerList.propTypes= {
    data: PropTypes.arrayOf(PropTypes.shape({
        team: PropTypes.string.isRequired,
        wins: PropTypes.number.isRequired,
        defeats: PropTypes.number.isRequired,
        draws: PropTypes.number.isRequired,
        points: PropTypes.number.isRequired,
        id: PropTypes.string.isRequired
    })),
    listPlayers: PropTypes.func,
    player: PropTypes.object,
    pages: PropTypes.number,
    changePage: PropTypes.func
}

ContainerList.contextTypes = {
    t: PropTypes.func.isRequired
}