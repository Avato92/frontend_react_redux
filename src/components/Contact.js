import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import {
  UPDATE_FIELD_EMAIL_CONTACT,
  UPDATE_FIELD_SUBJECT_CONTACT,
  UPDATE_FIELD_COMMENT_CONTACT,
  SEND_EMAIL,
  SHORT_EMAIL,
  SHORT_SUBJECT,
  SHORT_COMMENT,
  WRONG_EMAIL,
  EMAIL_SENT,
  RE
} from '../constants/actionTypes';
/**
 *Import const RegEx for email
 */
const re = RE;

const mapStateToProps = state => ({ ...state.contact });

const mapDispatchToProps = dispatch => ({
  /**
   * Form validation
   * First email length, all clicks we check
   * Second subject length
   * Third comment length
   * And finally, we check email with RegEx
   */
  onChangeEmail: value =>{
    (value.length <= 6 )?
      dispatch({ type: SHORT_EMAIL, key: 'email', value}):
      dispatch({ type: UPDATE_FIELD_EMAIL_CONTACT, key: 'email', value });
  },
  onChangeSubject : value =>{
    (value.length <= 6)?
      dispatch({ type: SHORT_SUBJECT, key: 'subject', value}):
      dispatch({ type: UPDATE_FIELD_SUBJECT_CONTACT, key: 'subject', value});
  },
  onChangeComment: value =>{
    (value.length <= 6)?
      dispatch({ type: SHORT_COMMENT, key: 'comment', value}):
      dispatch({ type: UPDATE_FIELD_COMMENT_CONTACT, key: 'comment', value});
  },
  onSubmit: (email, subject, comment) =>{
    if(!re.test(String(email).toLowerCase())){
      dispatch({ type: WRONG_EMAIL});
    }else{
      dispatch({ type: SEND_EMAIL, payload: agent.Contact.sendEmail(email, subject, comment) });
      dispatch({type: EMAIL_SENT });
    }
  }
});

class Contact extends React.Component{
  constructor(){
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changeSubject = ev => this.props.onChangeSubject(ev.target.value);
    this.changeComment = ev => this.props.onChangeComment(ev.target.value);
    this.submitForm = (email, subject, comment) => ev => {
      ev.preventDefault();
      this.props.onSubmit(email, subject, comment);
    };
  }
  render(){
    
    return (
        <div>
          <form onSubmit={this.submitForm(this.props.email, this.props.subject, this.props.comment)}>
          <fieldset>

            <fieldset className="form-group">
              <input
                className="form-control form-control-lg"
                placeholder="Email"
                value={this.props.email}
                onChange={this.changeEmail} />
              <span hidden={!this.props.wrongEmail}><font size="2" color="red">{this.props.errorEmail}</font></span>
            </fieldset>

            <fieldset className="form-group">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Subject"
                value={this.props.subject}
                onChange={this.changeSubject} />
                <span hidden={!this.props.wrongSubject}><font size="2" color="red">{this.props.errorSubject}</font></span>
            </fieldset>

            <fieldset className="form-group">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Comment"
                value={this.props.comment}
                onChange={this.changeComment} />
                <span hidden={!this.props.wrongComment}><font size="2" color="red">{this.props.errorComment}</font></span>
            </fieldset>

            <button
              className="btn btn-lg btn-primary pull-xs-right"
              type="submit"
              disabled={this.props.wrongEmail || this.props.wrongSubject || this.props.wrongComment }>
              Send Email
            </button>

          </fieldset>
        </form>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
