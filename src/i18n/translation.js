import english from "./english.json";
import spanish from "./spanish.json";
import valencian from "./valencian.json";

export const translations = {
    "en": english,
    "es": spanish,
    "ca": valencian
}