import english from "./i18n/english.json";
import spanish from "./i18n/spanish.json";
import valencia from "./i18n/valencian.json";
import { Settings } from "./settings";

let userLanguage = spanish;

let Utils = {
  /**  We set a language for user and store the same as a cookie afterwards we reload the page to apply changes
   * If there is no lang we set english as the default language for user
   * @param {string} lang - Choosen language.
   */
  setUserLanguage: function(lang = "") {
    if (!lang) {
      let language = this.getCookie("language");
      if (!language || language === "") {
        var userLang = navigator.language; //Get navigator locales

        userLang = userLang.substring(0, 2).toUpperCase();

        switch (userLang) {
          case "EN":
            lang = "english";
            break;
          case "ES":
            lang = "spanish";
            break;
          case "CA":
            lang = "valencia";
            break;
          default:
            lang = Settings.defaultLanguage;
        }
        this.setCookie("language", lang, 365);
      } else {
        lang = this.getCookie("language");
      }
    } else {
      if (this.getCookie("language") !== lang) {
        this.setCookie("language", lang, 365);
      }
    }
    switch (lang) {
      case "spanish":
        userLanguage = spanish;
        break;
      case "valencia":
        userLanguage = valencia;
        break;
      default:
        userLanguage = english;
    }
  },
  getUserLang: function() {
    switch (this.getCookie("language")) {
      case "en":
        return "en";
      case "ca":
        return "va";
      case "es":
        return "es";
      default:
        return "es";
    }
  },
  /**
   * Get cookie by name
   * @param {string} cname
   */
  getCookie: function(name) {
    try {
      var value = "; " + document.cookie;
      var parts = value.split("; " + name + "=");
      if (parts.length === 2)
        return parts
          .pop()
          .split(";")
          .shift();
    } catch (e) {
      return "";
    }
  },
  /**
   * Create new cookie
   * @param {string} cname  Cookie name
   * @param {string} cvalue  Cookie value
   * @param {number} exdays Cookie expiration in days
   */
  setCookie: function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  },
  deleteCookie: function(name) {
    document.cookie =
      name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  }
};
export { Utils };
