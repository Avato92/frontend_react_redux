import {
    UPDATE_FIELD_EMAIL_CONTACT,
    UPDATE_FIELD_SUBJECT_CONTACT,
    UPDATE_FIELD_COMMENT_CONTACT,
    SEND_EMAIL,
    SHORT_EMAIL,
    SHORT_COMMENT,
    SHORT_SUBJECT,
    WRONG_EMAIL,
    TOASTR_OPTIONS
  } from '../constants/actionTypes';
  import toastr from 'toastr';
   /* Import Toastr options from actions, because we want to use it for the whole application
  */
  toastr.options = TOASTR_OPTIONS;

  const INITIAL_STATE = {
    inProgress : false,
    wrongEmail : true,
    wrongSubject : true,
    wrongComment : true,
  }
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case SEND_EMAIL:
        (action.error)? toastr['error']('Fail! Please try later!'): toastr["success"]("Congrats! E-mail sent!")
        return {
          ...state,
          inProgress: false,
          errors: action.error ? action.payload.errors : null
        };
      case UPDATE_FIELD_EMAIL_CONTACT:
        return { 
          ...state,
          [action.key]: action.value,
          wrongEmail : false,
          };
      case UPDATE_FIELD_SUBJECT_CONTACT:
        return { 
          ...state,
          [action.key]: action.value,
          wrongSubject : false,
      };
      case UPDATE_FIELD_COMMENT_CONTACT:
        return { 
          ...state,
          [action.key]: action.value,
          wrongComment : false,
      };
      case SHORT_EMAIL:
        return { 
          ...state,
          [action.key]: action.value,
          wrongEmail : true,
          errorEmail : 'Email too short'
        };
      case SHORT_SUBJECT:
        return {
          ...state,
          [action.key]: action.value,
          wrongSubject : true,
          errorSubject : 'Subject must contain at least 6 letters'
        };
      case SHORT_COMMENT:
        return {
          ...state,
          [action.key]: action.value,
          wrongComment : true,
          errorComment : 'Comment must contain at least 6 letters'
        };
      case WRONG_EMAIL:
        return { 
          ...state,
          [action.key]: action.value,
          wrongEmail : true,
          errorEmail : 'Enter a valid email'
        };
      default:
        return state;
    }
  };