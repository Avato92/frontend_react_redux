import {
    LIST_TEAMS,
    LIST_SPORTS,
    LIST_CATEGORIES,
    LIST_PLAYERS,
    ASYNC_START,
    UPDATE_DROPDOWN,
    CHANGE_PAGE
} from '../constants/actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case LIST_SPORTS:
            return {
                ...state,
                sports: action.payload[0].sports
            }
        case LIST_CATEGORIES:
            return {
                ...state,
                categories: action.payload.sports[0].categories
            }
        case LIST_TEAMS:
            const data = action.payload.category.team.map(team => {
                return { team: team.name, wins: team.wins, draws: team.draws, defeats: team.defeats, points: team.points, id: team.id }
            })
            return {
                ...state,
                teams: data,
                isLoading: false,
                teamSelected: action.value
            };
        case UPDATE_DROPDOWN:
            return {
                ...state,
                [action.key]: action.value
            }
        case LIST_PLAYERS:
            return{
                ...state,
                pages: action.payload.size[0].players.length,
                player: action.payload.player.team.players[0]
            }
        case CHANGE_PAGE:
            return{
                ...state,
                player: action.payload.team.players[0]
            }
        case ASYNC_START:
            if (action.subtype === LIST_TEAMS) {
                return {
                    ...state,
                    isLoading: true
                }
            } else {
                return {
                    ...state
                }
            }
        default:
            return state;
    }
}